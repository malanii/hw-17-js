function fib(f0, f1, n) {
    if (n == 1 || n == 2) {
        return 1;
    } else {
        for (let i = 3; i <= n; i++) {
            let ans = f0 + f1;
            f0 = f1;
            f1 = ans;
        }
    }
    return f1;
}

let myN = +prompt("Please, enter number");

console.log(fib(1, 1, myN));